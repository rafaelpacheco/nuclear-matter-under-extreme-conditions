# Nuclear Matter Under Extreme Conditions

This repository is a set of codes used in study of QCD matter under extreme conditions using NJL effective model.
The calculation codes are in Fortran and the plot codes in Python.

      program gpe_njlsu2_mag
C       Rafael Pacheco - 2020, UFSC
C       Units on GeV
      implicit none
      logical check
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, xacc, pi, pi2, eB, xmef_I
      real*8 xmef_S, M, meff, mu, f_gap_1
      real*8 f_gap_2, f_gap_3, Omega_1, Omega_2, zriddr,xaux
      real*8 Omega_3, Int_L, sigma, M_1, M_2, Omega
      real*8 h, M1, M2, eBff
      real*8 Omega1, Omega2, Omega3
      real*8 M_gap1, M_gap2, M_gap3
      integer i, nstep, JJ, ip
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/mag_field_eff/eBff
      common/ctes/hc,RM     
      external f_gap_1, Omega_1, Omega_2, Omega_3, zriddr, FISRT0, Int_L
      external f_gap_2, f_gap_3, M_1, Omega 
      external Int_L_th
      external Omega1, Omega2, Omega3
      RM = 1000d0
      hc=197.326d0
      Nc = 3d0
      Nf = 2d0
      xacc = 1d-10
      pi = dacos(-1.d0)
      pi2 = pi*pi 
      h = 1d-3
      write(6,*)'1 => Buballa, 2 => Ricardo'
      read(5,*)ip
      if (ip.eq.1) then
        G=2.44d0 
        Lambda=587.9d0  
        m_=5.6d0/RM 
        Lambda=Lambda/RM
        G=G/Lambda**2.d0
       endif
      if(ip.eq.2) then
       Lambda=664.3d0 
       m_=5.6d0/RM      
       G=4.5d0
       Lambda=Lambda/RM
      endif

 
!      eB = 0d0
!      do while (eB.lt.1d0)
!        eB = eB + 1d-2
!        M_gap1 = zriddr(f_gap_1, xacc, Lambda, xacc)
!        M_gap2 = zriddr(f_gap_1, xacc, Lambda, xacc)
!        M_gap3 = zriddr(f_gap_1, xacc, Lambda, xacc)
!        write(*,"(9999(G12.5,:,' '))")eB, M_gap1, M_gap2, M_gap3 
!      enddo 

!      eB = 0.4d0
!      do while (M.lt.1d0)
!        M = M + 1d-3
!        meff = M
!        call GAUSS(Int_L,0d0,1d0, 50, sigma, JJ)  
!        write(*,"(9999(G12.5,:,' '))")M, Omega1(M), Omega2(M),
!     & Omega3(M)+sigma
!      enddo
      
!      meff = M
!      eBff = eB
!      print*, Omega1(M), Omega2(M)
!      print*, Omega_1(eB), Omega_2(eB)

!      do while (eB.lt.1d0)
!        eB = eB + 1d-2
!        M = 0.2d0
!        meff = M
!      print*, eB, Omega_1(eB), Omega_2(eB) 
!      enddo

!      do while (M.lt.1d0)
!        M = M + 1d-2
!        eB = 0.4d0
!        eBff = eB
!        print*, M, Omega1(M), Omega2(M) 
!      enddo


      eB = 0d0
      M = 0.2d0
      do while (eB.lt.1d0)
        eB = eB + 1d-2
       meff = M
        eBff = eB
        M1 = -(1d0/(12d0*eB*h))*(8*(Omega_1(eB*(M+h))-Omega_1(eB*(M-h)))
     & -Omega_1(eB*(M+2*h))+Omega_1(eB*(M-2*h)))
        M2 = -(1d0/(12d0*eB*h))*(8*(Omega_2(eB*(M+h))-Omega_2(eB*(M-h)))
     & -Omega_2(eB*(M+2*h))+Omega_2(eB*(M-2*h)))
      write(6,200) eB, M_1(eB), M_2(eB), M1, M2,
     & Omega_1(eB), Omega_2(eB)

 200  format(f8.4,2x,f10.4,2x,e14.7,2x,e14.7,2x,e14.7,2x,e14.7,2x,e14.7)

      enddo
! how to ask to save output?

      stop
      end

C
C------------------------------------------------------------------
C
      real*8 function Omega_1(eB)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, gamma_e
      real*8 derz, alpha
      real*8 eBff
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field_eff/eBff
      common/ctes/hc,RM
      external derz, alpha
      gamma_e = 0.5772156649d0
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      M = meff
      P_mag = 0d0
      P_fmag = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        P_mag = derz(xf)-.5*(xf**2-xf)*dlog(xf)+(xf**2)/4
        P_fmag = P_fmag + Nc*(cq(f)*eB)**2/(2*pi2)*P_mag
!     & + Nc*(cq(f)*eB)**2/(8*pi2)*(1d0/3d0)*(dlog(meff**2/Lambda**2)
!     & + gamma_e)  
      enddo
      Omega_1=((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) - P_fmag 
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function Omega_2(eB)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, gamma_e
      real*8 derz, alpha
      real*8 eBff
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field_eff/eBff
      common/ctes/hc,RM
      external derz, alpha
      gamma_e = 0.5772156649d0
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      M = meff
      P_mag = 0d0
      P_fmag = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        P_mag = derz(xf)-.5*(xf**2-xf)*dlog(xf)+(xf**2)/4
     &          -(1d0/12d0)*(1d0+dlog(xf))
        P_fmag = P_fmag + Nc*(cq(f)*eB)**2/(2*pi2)*P_mag
!     & + Nc*(cq(f)*eB)**2/(8*pi2)*(1d0/3d0)*(dlog(meff**2/Lambda**2)
!     & + gamma_e)  
      enddo
      Omega_2=((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) - P_fmag 
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function M_1(eB)
      implicit none
      real*8 gammln
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, eBff, gamm_E
      real*8 derz, alpha
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field_eff/eBff
      common/ctes/hc,RM
      external derz, alpha, gammln
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      gamm_E = 0.5772156649d0
      M = meff
      M_1 = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        M_1 = M_1 + Nc*(cq(f))**2*eB/(2*pi2)*(
     & 2*derz(xf) +
     & .5*xf*(dlog(xf)-xf-2*gammln(xf)+dlog(2*pi))    
     & - (1d0/6d0)*(dlog(M**2/Lambda**2)+gamm_E) )
      enddo
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function M_2(eB)
      implicit none
      real*8 gammln
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, eBff, gamm_E
      real*8 derz, alpha
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field_eff/eBff
      common/ctes/hc,RM
      external derz, alpha, gammln
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      gamm_E = 0.5772156649d0
      M = meff
      M_2 = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        M_2 = M_2 + Nc*(cq(f))**2*eB/(2*pi2)*(
     & 2*derz(xf) +
     & .5*xf*(dlog(xf)-xf-2*gammln(xf)+dlog(2*pi))   
     & - (1d0/6d0)*(dlog(Lambda**2*xf/M**2)-gamm_E +.5) )
      enddo
      return
      end 

C
C-------------------------------------------------------------
C
      real*8 function f_gap_3(M)
      implicit none
      real*8 gammln, sigma, Int_L
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB, f_e
      real*8 M, meff, cq(2), xf, f_vac, f_mag_, f_mag, mu, E_L
      integer JJ ,f
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external gammln, Int_L
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      meff = M
      f_vac = m_ - M + M*G*Nc*Nf/(pi2)*(Lambda*E_L
     $     -M**2*dlog((Lambda+ E_L)/M) )
      CALL GAUSS(Int_L,0d0,1d0, 50, sigma, JJ)  
      f_e = 0d0
      do f = 1,2
       xf = M**2/(2.d0*cq(f)*eB)
       f_e = f_e + M*cq(f)*eB*Nc*G/pi2*(1./(12*xf))
      enddo
      f_mag = -M*G/(2.*cq(f)*eB)*sigma 
      f_gap_3 = f_vac+f_e+f_mag!+f_e
      return
      end

C
C-------------------------------------------------------------
C
      real*8 function f_gap_2(M)
      implicit none
      real*8 gammln
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, cq(2), xf, f_vac, f_mag_, f_mag, mu, E_L
      integer f
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external gammln
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      f_vac = m_ - M + M*G*Nc*Nf/(pi2)*(Lambda*E_L
     $     -M**2*dlog((Lambda+ E_L)/M) )
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        f_mag_= M*cq(f)*eB*Nc*G/(pi2)*(gammln(xf)-0.5d0*dlog(2.d0*pi)+xf
     $     -0.5*(2*xf-1)*dlog(xf) )!-1./(12*xf))
        f_mag = f_mag + f_mag_ 
      enddo
      f_gap_2 = f_vac+f_mag
      return
      end

C
C-------------------------------------------------------------
C
      real*8 function f_gap_1(M)
      implicit none
      real*8 gammln
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, cq(2), xf, f_vac, f_mag_, f_mag, mu, E_L
      integer f
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external gammln
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      f_vac = m_ - M + M*G*Nc*Nf/(pi2)*(Lambda*E_L
     $     -M**2*dlog((Lambda+ E_L)/M) )
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        f_mag_= M*cq(f)*eB*Nc*G/(pi2)*(gammln(xf)-0.5d0*dlog(2.d0*pi)+xf
     $     -0.5*(2*xf-1)*dlog(xf))+ M*cq(f)*eB*Nc*G/pi2*(1./(12*xf))
        f_mag = f_mag + f_mag_  
      enddo
      f_gap_1 = f_vac+f_mag
      return
      end
C
C-------------------------------------------------------------
C
      real*8 function Omega1(M)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, gamma_e
      real*8 derz, alpha
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external derz, alpha
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      gamma_e = 0.5772156649d0
      P_mag = 0d0
      P_fmag = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        P_mag = derz(xf)-.5*(xf**2-xf)*dlog(xf)+(xf**2)/4
        P_fmag = P_fmag + Nc*(cq(f)*eB)**2/(2*pi2)*P_mag
     & + Nc*(cq(f)*eB)**2/(8*pi2)*(1d0/3d0)*(dlog(M**2/Lambda**2)
     & + gamma_e)  
      enddo
      Omega1=((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) - P_fmag 
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function Omega2(M)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, gamma_e
      real*8 derz, alpha
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external derz, alpha
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      gamma_e = 0.5772156649d0
      P_mag = 0d0
      P_fmag = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        P_mag = derz(xf)-.5*(xf**2-xf)*dlog(xf)+(xf**2)/4
     &          -(1d0/12d0)*(1d0+dlog(xf))
        P_fmag = P_fmag + Nc*(cq(f)*eB)**2/(2*pi2)*P_mag
     & + Nc*(cq(f)*eB)**2/(8*pi2)*(1d0/3d0)*(dlog(M**2/Lambda**2)
     & + gamma_e)  
      enddo
      Omega2=((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) - P_fmag 
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function Omega3(M)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f
      real*8 derz, alpha, sigma
      integer f, k, k_fmax, JJ
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external derz, alpha, Int_L
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      E_L = dsqrt(Lambda**2+M**2)
      P_mag = 0d0
      P_fmag = 0d0
      Omega3=((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) 
      return
      end 

C
C------------------------------------------------------------------
C
      real*8 function Int_L(t)
      implicit none
      real*8 alpha, s, cq(2), meff, eB, xf, Nc, pi, pi2
      real*8 t, ds, Lambda, gamma_e 
      integer f
      common/mag_field/eB
      common/eff_mass/meff
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      Lambda = 587.9d0/1d3
      gamma_e = 0.5772156649d0
      Nc = 3
      pi = dacos(-1.d0)
      pi2 = pi*pi 
      s = t/(1-t)
      ds = 1/(1-t)**2
      Int_L = 0d0
      do f=1,2
        xf = meff**2/(2.d0*cq(f)*eB)
        Int_L = Int_L - Nc*(cq(f)*eB)**2/(8*pi2)*
     & ((dexp(-2d0*xf*s)/s**2)*(1d0/dtanh(s)-1d0/s-s/3d0)*ds)!+
!     & (1d0/3d0)*dlog(meff**2/Lambda**2)+gamma_e)
      enddo
      end
C
C------------------------------------------------------------------
C
      real*8 function Int_L_th(s)
      implicit none
      real*8 alpha, s, cq(2), meff, eB, xf, Nc, pi, pi2
      integer f
      common/mag_field/eB
      common/eff_mass/meff
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
      Nc = 3
      pi = dacos(-1.d0)
      pi2 = pi*pi 
      Int_L_th = 0d0
      do f=1,2
        xf = meff**2/(2.d0*cq(f)*eB)
        if(s.lt.1d-5)then 
                Int_L_th = Int_L_th - Nc*(cq(f)*eB)**2/(8*pi2)*
     & (dexp(-2d0*xf*s)/s**2)*(- s**3d0/45d0 + 2d0*s**5d0/945d0
     & - s**7d0/4725d0 + s**9d0/93555d0)
        else if(s.gt.1d-5)then
                Int_L_th = Int_L_th - Nc*(cq(f)*eB)**2/(8*pi2)*
     & (dexp(-2d0*xf*s)/s**2)*(1d0/dtanh(s)-1d0/s-s/3d0)
        endif
      enddo
      end
C
C-------------------------------------------------------------
C
      real*8 function Omega(M)
      implicit none
      real*8 RM, hc, Lambda, m_, G, Nc, Nf, pi, pi2, eB
      real*8 M, meff, mu, E_L, cq(2), xf, P_mag, P_fmag, P_med, P_fmed
      real*8 rk_fmax, s_f, gamma_e, P_aux
      real*8 derz, alpha
      integer f, k, k_fmax
      common/cdata/Lambda, m_, G, Nc, Nf, pi, pi2
      common/eff_mass/meff
      common/chem_pot/mu
      common/mag_field/eB
      common/ctes/hc,RM
      external derz, alpha
      cq(1) = 2.d0/3.d0
      cq(2) = 1.d0/3.d0
!      M = meff
      E_L = dsqrt(Lambda**2+M**2)
      gamma_e = 0.577215664901532d0
      P_mag = 0d0
      P_fmag = 0d0
      do f=1,2
        xf = M**2/(2.d0*cq(f)*eB)
        P_mag = derz(xf)-.5*(xf**2-xf)*dlog(xf)+(xf**2)/4
!     &          -(1/12)*(1+dlog(xf))
        P_fmag = P_fmag + Nc*(cq(f)*eB)**2/(2*pi2)*P_mag
!        P_aux =P_aux+(Nc/3)*(cq(f)*eB)**2/(8*pi2)*(dlog(M**2/Lambda**2))
!     &   +gamma_e)
      enddo
      P_med = 0d0
      P_fmed = 0d0
      s_f = 0d0
      do f = 1,2
        if (mu.gt.M) then
        rk_fmax=(mu**2.d0-M**2.d0)/(2.d0*cq(f)*eB)
        k_fmax=dint(rk_fmax)
        do k = 0, k_fmax
          s_f = dsqrt(M**2.d0+2.d0*cq(f)*eB*k)
          P_med = mu*dsqrt(mu**2-s_f**2)-
     &            s_f**2*dlog((mu+dsqrt(mu**2.d0-s_f**2.d0))/s_f)
          P_fmed = P_fmed + alpha(k)*(cq(f)*eB*Nc)/(4*pi2)*P_med
        enddo
        endif
      enddo

      Omega = ((M-m_))**2/(4*G)+(Nc*Nf)/(8*pi2)*(M**4*dlog((Lambda+E_L)/
     & M)-E_L*Lambda*(Lambda**2+E_L**2)) - P_fmag - P_fmed !- P_aux 
!      Omega = Omega*(-1)
      return
      end 
C
C-------------------------------------------------------------
C
      real*8 function alpha(k)
      implicit none
      real*8 a
      integer  k
         if(k.eq.0)then
          a = 1.d0                               
         else if(k.ne.0)then
          a = 0.d0 
         end if
         alpha = dint(2.d0 - a)
      return
      end


C
C------------------------------------------------------------------
C
      real*8 function derz(x)
      implicit none
      real*8 x,x_0,pi,xa,xa1,gammln,FXINT,dzx0
      integer ii
      external gammln
      pi=dacos(-1.d0)
C  
C   valor conhecido para der(zeta(-1,x_0)) (calculado pelo Matematica)
C        x_0=0.1d0
C        der(zeta(-1,x_0))=0.0253221
C
       x_0=0.1d0
       dzx0=0.0253221d0
C
C     0 < x < 0.1
C
      if(x.gt.0.d0 .and. x.lt.0.1d0) then
        xa=x+1.d0
        xa1=-0.5d0*(xa-x_0)*(1.d0-(xa+x_0)+dlog(2.d0*pi))
C 
        call GAUSS(gammln,x_0,xa,10,FXINT,II)
        derz=xa1+FXINT+dzx0 -x*dlog(x)
      else
C
        xa1=-0.5d0*(x-x_0)*(1.d0-(x+x_0)+dlog(2.d0*pi))
C 
        call GAUSS(gammln,x_0,x,10,FXINT,II)
        derz=xa1+FXINT+dzx0
      endif
C
      return
      end
C
C-------------------------------------------------------------
C
      subroutine  FIRST0(xmef_I,xmef_S,nstep,func,xmef)
      implicit none
      real*8 step,step1,xmef1,meff,oldsgn,oldsgn1,ff,f,xmef_S,xmef_I
      real*8 sgn,sgn1,func,rmun,RM,hc,xmef
      integer nr,nstep,jm,j
      logical sign,sign1 
      common/ctes/hc,RM     
      external FUNC
C  cuidado com estes limites se forem alterados podem calcular xmef00 mal 
      xmef_I=xmef_I/RM
      xmef_S=xmef_S/RM
C      nstep=400
      step=(xmef_S-xmef_I)/dfloat(nstep)
C      
            nr=0
            sign=.true.
            do 12  j=1,nstep+1
               xmef=dfloat(j-1)*step +2.d0/RM
c-----------------------------------------------------
            f=func(xmef)
C            write(6,*)'xmef=',xmef,' f=',f
            sgn=f/abs(f)
            sign=j.eq.1.or.oldsgn.eq.sgn
            if(.not.sign)then
c
               xmef1=xmef-step
               step1=step*0.1d0
c-----------------------------------------------------
c     loop em x para procurar solucoes: refinamento
c
               do 13 while(step1.gt.1.d-12)
                  sign1=.true.
                  jm=1        
                  do 14  while(jm.lt.20)
                     xmef=xmef1+(jm-1)*step1
c-----------------------------------------------------
                     ff=func(xmef)
C                     write(6,*)'xmef=',xmef,' ff=',ff
                     sgn1=ff/abs(ff)
                     sign1=jm.eq.1.or.oldsgn1.eq.sgn1
                     if(.not.sign1)then
                        xmef1=xmef-step1
                        step1=step1*0.1d0
                        goto 13
                     end if
                     oldsgn1=sgn1
                     jm=jm+1
 14               continue
                  if (sign1) write(6,*)'no solution '
                  if (sign1) goto 12
 13            continue
               nr=nr+1
C  SAIDA 
             if(nr.eq.1) then
              meff=xmef
C              write(6,*)'mu=',rmun*RM,' meff=',meff*RM
              return
             endif
           end if
            oldsgn=sgn
 12      continue	
            nr=0
C          write(6,*)'nr=',nr,' meff=',xmef

       return
       end
C--------------------------------------------------------------------------

! NR-Fortran routines

      FUNCTION zriddr(func,x1,x2,xacc)
      IMPLICIT NONE
      INTEGER MAXIT
      REAL*8 zriddr,x1,x2,xacc,func,UNUSED
      PARAMETER (MAXIT=60,UNUSED=-1.11D30)
      EXTERNAL func
CU    USES func
      INTEGER j
      REAL*8 fh,fl,fm,fnew,s,xh,xl,xm,xnew
      fl=func(x1)
      fh=func(x2)
      if((fl.gt.0..and.fh.lt.0.).or.(fl.lt.0..and.fh.gt.0.))then
        xl=x1
        xh=x2
        zriddr=UNUSED
        do 11 j=1,MAXIT
          xm=0.5d0*(xl+xh)
          fm=func(xm)
          s=dsqrt(fm**2-fl*fh)
          if(s.eq.0.)return
          xnew=xm+(xm-xl)*(sign(1.d0,fl-fh)*fm/s)
          if (dabs(xnew-zriddr).le.xacc) return
          zriddr=xnew
          fnew=func(zriddr)
          if (fnew.eq.0.) return
          if(sign(fm,fnew).ne.fm) then
            xl=xm
            fl=fm
            xh=zriddr
            fh=fnew
          else if(sign(fl,fnew).ne.fl) then
            xh=zriddr
            fh=fnew
          else if(sign(fh,fnew).ne.fh) then
            xl=zriddr
            fl=fnew
          else
           write(6,*)'never get here in zriddr'
             stop
C            pause 'never get here in zriddr'
          endif
          if(dabs(xh-xl).le.xacc) return
11      continue
        write(6,*)'zriddr exceed maximum iterations'
         stop
C        pause 'zriddr exceed maximum iterations'
      else if (fl.eq.0.) then
        zriddr=x1
      else if (fh.eq.0.) then
        zriddr=x2
      else
C       write(6,*)'root must be bracketed in zriddr' !!!WARNING RIDDER DONT
c							CATCH THE ROOTS
       continue
C        pause 'root must be bracketed in zriddr'
      endif
      return
      END
C
C-------------------------------------------------------------
C
        SUBROUTINE GAUSS(F,UG,OG,NN,FXINT,II)
        IMPLICIT DOUBLE PRECISION(A-H,O-Z)
        DIMENSION WG(10),ZG(10)
        DATA WG/0.6667134430869 D-01,
     C  0.1494513491506D00,
     C  0.2190863625160D00,
     C  0.2692667193100D00,
     C  0.2955242247148D00,
     C  0.2955242247148D00,
     C  0.2692667193100D00,
     C  0.2190863625160D00,
     C  0.1494513491506D00,
     C           0.6667134430869D-01/
        DATA ZG/-0.9739065285172D00,
     C  -0.8650633666890D00,
     C  -0.6794095682990D00,
     C  -0.4333953941292D00,
     C  -0.1488743389816D00,
     C  +0.1488743389816D00,
     C  +0.4333953941292D00,
     C  +0.6794095682990D00,
     C  +0.8650633666890D00,
     C          +0.9739065285172D00/

        FXINT=0.D0
        HH=(OG-UG)/DBLE(FLOAT(NN))
        U=UG
        O=U+HH
        KK=1
24      OU=O+U
        RI=0.D0
        DO 26 I=1,10
        X=0.5D0*(ZG(I)*HH+OU)
        FUNCAO=F(X)
26      RI=RI+WG(I)*FUNCAO
        FXINT=RI*HH/2.D0+FXINT
        KK=KK+1
        IF(KK-NN)28,28,9999
28      U=O
        O=O+HH
        GO TO 24
9999    RETURN
        END

C
C--------------------------------------------------------------------
C
      REAL*8 FUNCTION gammln(XX)
      REAL*8 XX
      INTEGER j
      REAL*8 ser,stp,tmp,x,y,cof(6)
      SAVE cof,stp
      DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,
     *24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,
     *-.5395239384953d-5,2.5066282746310005d0/
      x=xx
      y=x
      tmp=x+5.5d0
      tmp=(x+0.5d0)*log(tmp)-tmp
      ser=1.000000000190015d0
      do 11 j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
11    continue
      gammln=tmp+log(stp*ser/x)
      return
      END
C
C--------------------------------------------------------------
C

import numpy as np
import matplotlib.pyplot as plt

a = np.loadtxt('njlsu2.dat')
#plt.plot(a[:,0], a[:,1], '.', label = r'$\mathcal{M}_1$ analytical')
plt.plot(a[:,0], a[:,2], '.', label = r'$\mathcal{M}_2$ analytical')
#plt.plot(a[:,0], a[:,3], '.', label = r'$\mathcal{M}_1$ numerical')
plt.plot(a[:,0], a[:,4], '.', label = r'$\mathcal{M}_2$ numerical')
#plt.plot(a[:,0], a[:,5], '.', label = r'$\Omega_1$')
#plt.plot(a[:,0], a[:,6], '.', label = r'$\Omega_2$')

#plt.plot(a[:,0], a[:,1], '.', label = r'$\Omega_1$')
#plt.plot(a[:,0], a[:,2], '.', label = r'$\Omega_2$')
#plt.plot(a[:,0], a[:,3], '.', label = r'$\Omega_3$')

#plt.plot(a[:,0], a[:,1], '-*', label = r'gap from $\Omega_1$')
#plt.plot(a[:,0], a[:,2], 'o', label = r'gap from $\Omega_2$')
#plt.plot(a[:,0], a[:,3], '*', label = r'gap from $\Omega_3$')

plt.xlabel(r'$eB (GeV^2)$')
plt.legend()
plt.grid()
plt.show()

